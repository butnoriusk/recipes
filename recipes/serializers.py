from rest_framework import serializers
from django.contrib.auth.models import User, Group
from .models import *

class UserSerializer(serializers.ModelSerializer): # HyperlinkedModelSerializer
    username = serializers.CharField(max_length=25)
    password = serializers.CharField(write_only=True, max_length=30)
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'email', 'groups', 'recipes', 'is_superuser']  

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate(self, data):
        username = data['username']
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError('Username already exists')
        return data

class RecipeSerializer(serializers.ModelSerializer):
    name = models.CharField(max_length=50)
    difficulty = models.CharField(max_length=2)
    duration = models.IntegerField()  
    status = models.BooleanField()    
    class Meta:
        model = Recipe
        fields = ('id', 'name', 'difficulty', 'duration', 'description', 'ingredients', 'user' ) # 'status' 'ratings'
        extra_kwargs = {
            'id': {
                'required': False,
            },
            'duration': {
                'required': False,
            }
        }

class IngredientSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50)
    class Meta:
        model = Ingredient     
        fields = ('id', 'name' )
        extra_kwargs = {
            'id': {
                'required': False,
            }
        }
        
class RatingSerializer(serializers.ModelSerializer):
    rating = serializers.IntegerField()
    class Meta:
        model = Rating
        fields = ('id', 'rating', 'recipe')  # 'user'
        extra_kwargs = {
            'id': {
                'required': False,
            }
        }
