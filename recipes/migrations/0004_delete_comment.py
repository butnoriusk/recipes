# Generated by Django 2.2.5 on 2019-12-18 09:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0003_auto_20191217_2159'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Comment',
        ),
    ]
