# Generated by Django 2.2.5 on 2019-12-18 11:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0006_rating_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rating',
            name='user',
        ),
    ]
