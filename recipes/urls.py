from django.urls import path, include
from rest_framework import routers
from . import views
from django.conf.urls import url

router = routers.DefaultRouter()
router.register(r'api/users', views.UserView)
router.register(r'api/recipes', views.RecipeView)
router.register(r'api/ingredients', views.IngredientView)
router.register(r'api/ratings', views.RatingView)

urlpatterns = [
     path('', include(router.urls)),  
     path('register/', views.register, name = 'register'),
     path('login/', views.login, name = 'login'),
     path('logout/', views.logout, name='logout'),

     path('home/', views.home, name='home'),
     path('recipes/', views.recipes, name = 'recipes'),
     path('recipes/<id>', views.recipesMethods, name = 'recipesMethods'),
     path('recipes/d/<id>', views.deleteRecipe, name = 'deleteRecipe'),   
     path('recipes/e/<id>', views.editRecipe, name = 'editRecipe'),   

     path('ingredients/', views.ingredients, name = 'ingredients'),
     path('ingredients/d/<id>', views.deleteIngredient, name = 'deleteIngredient'),   
     path('ingredients/e/<id>', views.editIngredient, name = 'editIngredient'),         
     
     path('users/', views.users, name='users'),
]

