from django.db import models
from django.contrib.auth.models import User, Group
from django.utils import timezone


class Ingredient(models.Model): 
    class Meta:
        ordering = ['name',]
      
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    DIFFICULTY_CHOICES = [
        ('1', 'Very easy'),
        ('2', 'Easy'),
        ('3', 'Medium'),
        ('4', 'Hard'),
        ('5', 'Very hard'),]

    name = models.CharField(max_length=50)
    difficulty = models.CharField(max_length=2, choices=DIFFICULTY_CHOICES)
    duration = models.IntegerField()    
    ingredients = models.ManyToManyField(Ingredient)
    description = models.CharField(max_length=1000, default="")
    status = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='recipes', on_delete=models.CASCADE)   
    def __str__(self):
        return self.name
   

class Rating(models.Model):
    RATING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        )

    rating = models.IntegerField(choices=RATING_CHOICES, default=0)
    recipe = models.ForeignKey(Recipe, related_name='rating', on_delete=models.CASCADE, default="")
    #user = models.ForeignKey(User, related_name='made_ratings',  on_delete=models.CASCADE, default="")  # need info who made rating

    def __str__(self):
        return str(self.rating)
