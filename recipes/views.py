from django.shortcuts import render, redirect, render_to_response
from rest_framework import viewsets, permissions
from .models import *
from .forms import *
from .serializers import *
from rest_framework.generics import RetrieveAPIView
from rest_framework import generics
from rest_framework.decorators import action
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate, login as loginUser, logout as logoutUser
from django.contrib.auth.models import User
from rest_framework.decorators import action, permission_classes
from rest_framework.decorators import permission_classes as permission
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.urls import reverse, reverse_lazy
import requests
from requests import *
import ast
from django.views.generic.edit import CreateView
import datetime
import jwt

class RecipeView(viewsets.ModelViewSet):
   # permission_classes = [permissions.IsAuthenticatedOrReadOnly]                     # -Only,]
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
    
    @action(detail=True)
    def ingredients(self, request, *args, **kwargs):
        recipe = self.get_object()
        ingredients = recipe.ingredients.all()
        return Response(IngredientSerializer(ingredients, many=True).data)

    @action(detail=True)
    def ratings(self, request, *args, **kwargs):
        recipe = self.get_object()
        ratings = recipe.ratings.all()
        return Response(RatingSerializer(ratings, many=True).data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)         


class IngredientView(viewsets.ModelViewSet):
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly] # IsAuthenticatedOrReadOnly
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer


class RatingView(viewsets.ModelViewSet):
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly] # IsAuthenticated
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)        

    #def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user != instance.user:
            return Response({'message': "Forbidden"}, status=403)
        self.perform_destroy(instance)
        return Response({'message': "Success"})


class UserView(viewsets.ModelViewSet):
    # permission_classes = [permissions.IsAdminUser] #IsAdminUser         reikalingas
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    @action(detail=True)
    def recipes(self, request, *args, **kwargs):
        user = self.get_object()
        recipes = user.recipes.all()
        return Response(RecipeSerializer(recipes, many=True).data)
    
    @action(detail=False, methods=["post"])
    @permission([IsAdminUser])
    def admin(self, request, *args, **kwargs):
        if not self.request.user.is_admin:
            return Response({'message': "Not Authorized"}, status=403)
        user_id = self.request.data.get('user')
        user = User.objects.filter(id=user_id)
        user.update(is_admin=True)
        return Response({'message': "Updated"})

# front end

def recipes(request):
     if(request.method == "GET"):    
        response = get('http://localhost:8000/api/recipes/')
        data = response.json
        response = get('http://localhost:8000/api/ingredients/')
        ingredients = response.json
        context = {'ingredients': ingredients, 'recipes' :  data}       
        return render(request, 'recipes.html', context)
    
     if(request.method =="POST"):
        user = request.user.id # arba user ne tiesiog id              pagetinti useri pagal id  
        
        name = request.POST['name']
        difficulty = request.POST['difficulty']
        duration = request.POST['duration']
        description = request.POST['description']
        ingredients = request.POST['ingredients']

        data = {'name':name,
                'difficulty':difficulty,
                'user': user,
                'duration': duration,
                'description': description,
                'ingredients': ingredients}
        post("http://localhost:8000/api/recipes/", data=data)
        return HttpResponseRedirect('http://localhost:8000/recipes/')


def recipesMethods(request, id):
    if(request.method == "GET"):
        response = get('http://localhost:8000/api/recipes/'+ id + '/')
        recipesData = response.json
        response = get('http://localhost:8000/api/ratings/')
        ratingsData = Rating.objects.all()
        response = get('http://localhost:8000/api/ingredients/')
        ingredientsData = response.json
               
        total = 0
        count = 0
        finalRating = 0
        for rating in ratingsData:     
            if(rating.recipe.id - int(id) == 0):
                total= total+rating.rating
                count=count +1
        if(total!=0):
            finalRating = total/count
        context = {'recipes': recipesData, 'ingredients' : ingredientsData, 'rating' : finalRating}        
        return render(request, 'oneRecipe.html', context)
    if(request.method == "DELETE"):
        delete('http://localhost:8000/api/recipes/' + id + '/')
        response = HttpResponse(status=status.HTTP_204_NO_CONTENT)
        response['Location'] = 'recipes.html'
    return HttpResponseRedirect('http://localhost:8000/recipes/')

def deleteRecipe(request, id):
    delete('http://localhost:8000/api/recipes/' + id + '/')
    response = HttpResponse(status=status.HTTP_204_NO_CONTENT)
    response['Location'] = 'recipes.html'
    return HttpResponseRedirect('http://localhost:8000/recipes/')

def editRecipe(request, id):
    if(request.method == "GET"):
        response = get('http://localhost:8000/api/recipes/' + id + '/')
        data = response.json       
        response = get('http://localhost:8000/api/ingredients/')
        ingredients = response.json
        context = {'editor': data, 'ingredients':ingredients}
        return render(request, 'edit_recipe.html', context)
    if(request.method == "POST"):
        name = request.POST['name']
        data = {'name':name}
        patch("http://localhost:8000/api/ingredients/" + id + "/", data=data)
        return HttpResponseRedirect('http://localhost:8000/ingredients/')

'''
def oneRecipe(request, id):
    response = get('http://localhost:8000/api/recipes/'+ id + '/')
    data = response.json
    context = {'recipes': data}
    return render(request, 'oneRecipe.html', context)
'''

def ingredients(request):
    if(request.method == "GET"):    
        response = get('http://localhost:8000/api/ingredients/')
        data = response.json
        context = {'ingredients': data}
        return render(request, 'ingredients.html', context)
    if(request.method =="POST"):
        name = request.POST['name']
        data = {'name':name}           
        post("http://localhost:8000/api/ingredients/", data=data)
        return HttpResponseRedirect('http://localhost:8000/ingredients/')    

def deleteIngredient(request, id):                                      
    delete('http://localhost:8000/api/ingredients/' + id + '/')
    response = HttpResponse(status=status.HTTP_204_NO_CONTENT)
    response['Location'] = 'ingredients.html'
    return HttpResponseRedirect('http://localhost:8000/ingredients/')

def editIngredient(request, id):
    if(request.method == "GET"):
        response = get('http://localhost:8000/api/ingredients/' + id + '/')
        data = response.json       
        context = {'editor': data}
        return render(request, 'edit_ingredient.html', context)
    if(request.method == "POST"):
        name = request.POST['name']
        data = {'name':name}
        patch("http://localhost:8000/api/ingredients/" + id + "/", data=data)
        return HttpResponseRedirect('http://localhost:8000/ingredients/')

def register(request):
    if(request.method == 'POST'):
        form = UserCreationForm(request.POST)
        if(form.is_valid()):
            form.save()         
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, passowrd=password)
            return redirect('login')
    else:        
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'reg.html', context)

def login(request):
    if(request.method == 'POST'):
            username = request.POST['username']
            password = request.POST['password']
            
            user = authenticate(username = username, password = password)
            if user:
                body = {'username': username, 'password': password}
                response = post('http://localhost:8000/api/token/', body)
                res = response.json()
                token = res['access']
                loginUser(request, user)
                response = HttpResponseRedirect(reverse('home'))
                response.set_cookie('Authorization', token)
                return response
    return render(request, 'login.html', {})

def logout(request):
    logoutUser(request)
    return render(request, 'login.html', {})

@permission_classes([IsAuthenticated])
def home(request):
    return render(request, 'index.html', {})

def users(request):
    if(request.method == "GET"):    
        response = get('http://localhost:8000/api/users/')
        usersData = response.json
        response = get('http://localhost:8000/api/recipes/')
        recipesData = response.json
        context = {'users': usersData, 'recipes' : recipesData}
        return render(request, 'users.html', context)
